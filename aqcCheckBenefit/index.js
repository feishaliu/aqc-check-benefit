
////////配置参数开始
var smtp =  "smtp.qq.com"    ;        // 固定;
var mailFrom =  "xxxxxxxxxx@qq.com"   ;//发送者 ;
var mailTo = "xxxxxxxxxx@qq.com"   ;//接收者 ;
var mailPwd =  ""     ;////授权码 不是密码;

var aqcookie = process.env.aqcCheckBenefitCookie ;//环境变量添加 aqcCheckBenefitCookie,  完整的Cookie

////////配置参数结束

const axios = require("axios");
var nodemailer = require("nodemailer");


//不需要的删掉
let benefitList={
	"AQ03006":"爱奇艺会员月卡",
	"AQ03007":"爱奇艺会员季卡",
	"AQ03008":"京东E卡50元",
	"AQ03009":"网盘会员月卡",
	"AQ03010":"网盘超级会员月卡"
}

const headers = {
	"Accept": "application/json, text/plain, */*",
	"Accept-Encoding": "gzip, deflate, br",
	"Accept-Language": "zh-CN,zh;q=0.9",
	"Connection": "keep-alive",
	"Host": "qiye.baidu.com",
	"referer": "https://qiye.baidu.com/usercenter",
	"Sec-Fetch-Dest": "empty",
	"Sec-Fetch-Mode": "cors",
	"Sec-Fetch-Site": "same-origin",
  "User-Agent":
    "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36",

  
  "cookie": aqcookie
};

checkBenefit();

async function checkBenefit() {
	
	let res = await get("https://qiye.baidu.com/usercenter/getBenefitStatusAjax")
	console.log("res.data："+res.data);
    var msg=[];
    for (var item of Object.keys(res.data)) {
        if(item in benefitList){
            if(res.data[item]){
               msg.push(benefitList[item]); 
            }
			console.log("状态："+item)
		}
       
    }
    var str=msg.join(",");
    if(str.length>0){
        emailTo("爱企查权益上架通知",str+" 有库存啦")   
    }
    console.log("msg"+msg.join(","))

}

 function get(url, method="get", data) {
    return new Promise(async (resolve) => {
        try {
            //let url = `https://aiqicha.baidu.com/${api}`
            if (method == "get") res = await axios.get(url, {
                headers
            })
            if (method == "post") res = await axios.post(url, data, {
                headers
            })
            if(res.data.status==0) console.log("    操作成功")
            else console.log("失败：    "+res.data.msg)
            resolve(res.data)
        } catch (err) {
            console.log(err)
        }
        resolve();
    });
}

function emailTo(subject,text,html,callback) {
    var transporter = nodemailer.createTransport({
        host: smtp,
        auth: {
            user: mailFrom,
            pass: mailPwd //授权码,通过QQ获取

        }
    });
    var mailOptions = {
        from: mailFrom, // 发送者
        to: mailTo, // 接受者,可以同时发送多个,以逗号隔开
        subject: subject, // 标题
    };
    if(text != undefined)
    {
        mailOptions.text =text;// 文本
    }
    if(html != undefined)
    {
        mailOptions.html =html;// html
    }

    var result = {
        httpCode: 200,
        message: '发送成功!',
    }
    try {
        transporter.sendMail(mailOptions, function (err, info) {
            if (err) {
                result.httpCode = 500;
                result.message = err;
                callback(result);
                return;
            }
            callback(result);
        });
    } catch (err) {
        result.httpCode = 500;
        result.message = err;
        callback(result);
    }

}